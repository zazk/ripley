function abrirPopup(){

	jQuery.fn.AG_popup({
		ancho: 800,  //Ancho del Popup
		alto: 450,  //Alto del Popup
		contenedor: '#id_popup1',  //elemento que sera Popup
		bloqueo:true, // para activar bloqueo
		efecto:false,
		botonCerrar:'#botonCerrar',  // boton para cerrar el Popup  por default es  '.AG_popupCerrar'
		transparenciaColor:'#000000',  // color de fondo del bloqueo 
		transparencia:0.8  // nivel de transparencia del bloqueo 
	}); 
	
}

$(window).load(function(){
	$('.verPopup').bind('click',function(e){
		e.preventDefault();
		var enlace=$(this).attr('href');
		console.log("si -> "+enlace);
		$('#id_iframe').html('<iframe src="'+enlace+'" frameborder="0" marginwidth="0" marginheight="0" style="width:800px;height:410px;"></iframe>');
		abrirPopup();
	})		   
})