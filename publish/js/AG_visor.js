(function($) {
	
	$.AG_visor = function(el, opc) {
		
		var base = this;

		base.$el = $(el).addClass('AG_visor');
		base.$el.data('AG_visor', base); // elemento para acceder a metodos publicos

		base.init = function(){

			base.opc = $.extend({}, $.AG_visor.porDefecto, opc);
			base.posicion = parseFloat(0);
			base.total = base.opc.desplazamiento;
			//base.total = parseFloat(0);
			base.totalElementos = base.$el.find(base.opc.elemento).length;
			
			//alert("total  "+$(base.opc.elemento).length);
			
			/*base.$el.find('> li').first().addClass('ini');
   			base.$el.find('> li').last().addClass('fin');
   			
   			base.$el.find('> li.ini').clone().appendTo(base.$el);//clone the first image and put it at the end
   			base.$el.find('> li.fin').clone().prependTo(base.$el);//clone the last image and put it at the front
   			
   			base.totalElementos=base.totalElementos+parseFloat(2);*/
			
			
			base.anchoElemento= $(base.opc.elemento).parent().width();
			base.desplazamiento = Number(base.total * base.anchoElemento);
			base.totalDesplazamiento = parseFloat(Math.ceil(parseFloat(base.totalElementos / base.total)));
			base.elementosRestantes = Number(base.totalElementos - base.opc.area);
			base.desplazar = (base.opc.desplazamiento == 1) ? base.elementosRestantes: parseFloat(base.totalDesplazamiento) - 1;
			
			//base.datos = new Array();
			base.posicionVisor = parseFloat(0);
			base.posicion = parseFloat(0);
			base.movIzq=parseFloat(0);
			base.movDer=parseFloat(1);
			
			base.carrusel();

			//if(base.opc.activarPosicionInicial==true) base.posicionInicial();
			if(base.opc.botonesCarrusel==true && base.totalElementos>1) base.activarBotones();
			
			if(base.opc.auto==true && base.totalElementos>1) {
				base.delay=parseFloat(0);
				base.moviendo = false;
				base.auto();
			}
			
		}
		
		base.auto = function(){
			
			$(base.opc.bloqueo).bind('mouseenter mouseleave',function(o){
				if(o.type=="mouseenter") base.desactivarDelay();
				if(o.type=="mouseleave") base.activarDelay(true);
			})

			base.activarDelay(true);
				
		}
		
		base.carrusel = function(){

			base.$el.find(base.opc.elemento).filter(function(i){
				
				$(this).bind('click',{pos: i},function(e){

					e.preventDefault();
					
					base.posicionVisor = e.data.pos;
					
					if(base.opc.auto==true && base.moviendo==false)	base.activarDelay(true);
					
		            var vector = this.className.split(" ");
		            base.datos(vector,base.posicionVisor);
		            
				})

				var datos = this.className.split(" ");
				var tipo = datos[1].substring(datos[1].length-3,datos[1].length);
				
				if(tipo!='jpg' && tipo!='gif' && tipo!='png' && tipo!='swf') $('<span class="icono_video">').appendTo($(this));
		
			})
			
		}
		
		base.visorPrincipal = function(archivo,previo,mensaje){
			
			$(base.opc.visor).hide();
			$(base.opc.visor).fadeIn();
			
			var tipo = archivo.substring(archivo.length-3,archivo.length);
			var youtube = "http://www.youtube.com/v/"+archivo.replace(/^[^v]+v.(.{11}).*/,"$1")+"?fs=1";
			
			var elegido;
			
			if(tipo=='flv' || tipo=='mp4' || tipo=='mp3') elegido='<object type="application/x-shockwave-flash" data="player/player.swf" width="'+base.opc.anchoVisor+'" height="'+base.opc.altoVisor+'" > <param name="wmode" value="opaque"> <param value="bin/player/player.swf" name="movie" /> <param name="allowScriptAccess" value="sameDomain" /> <param name="allowFullScreen" value="true" /> <param name="flashvars" value="file='+archivo+'&amp;image='+previo+'" /> <param name="quality" value="high" /> <p>Please get the Adobe Flash Player!<\/p> <\/object>';
			else if(tipo=='jpg' || tipo=='gif'|| tipo=='png') elegido='<img src="'+archivo+'" width="'+base.opc.anchoVisor+'" height="'+base.opc.altoVisor+'" alt="" />';
			else if(tipo=='swf') elegido='<object type="application/x-shockwave-flash" data="'+archivo+'" width="'+base.opc.anchoVisor+'" height="'+base.opc.altoVisor+'" > <param name="wmode" value="opaque"> <param value="'+archivo+'" name="movie" /> <param name="allowScriptAccess" value="sameDomain" /> <param name="allowFullScreen" value="true" /> <param name="quality" value="high" /> <p>Please get the Adobe Flash Player!<\/p> <\/object>';
			else elegido='<object type="application/x-shockwave-flash" data="'+youtube+'" width="'+base.opc.anchoVisor+'" height="'+base.opc.altoVisor+'" > <param value="'+youtube+'" name="movie" /> <param name="allowScriptAccess" value="sameDomain" /> <param name="allowFullScreen" value="true" /> <param name="quality" value="high" /> <param value="opaque" name="wmode" /> <p>Please get the Adobe Flash Player!<\/p> <\/object>';
				
			if(mensaje!="" && base.opc.mensaje==true) elegido=elegido+'<div class="mensaje"><p>'+mensaje+'</p></div>';
			
			$(base.opc.visor).html(elegido);
			console.log("si posicion  "+base.posicion+"    posicion Visor  "+base.posicionVisor);
		}
		
		base.datos = function(vector,posicionVisor){
			
			var mensaje = (base.opc.mensaje==true)? base.$el.find(base.opc.elemento).eq(posicionVisor).attr('title') : "";
            var previo = (vector[2]!=undefined)? vector[2] : "";
            var archivo = vector[1];
            
            base.visorPrincipal(archivo,previo,mensaje);
            
            base.$el.find(base.opc.elemento).removeClass('activo');
		    base.$el.find(base.opc.elemento).eq(posicionVisor).addClass('activo');
		    
		}
		
		base.posicionInicial = function(){
			
			var vector = base.$el.find(base.opc.elemento).eq(base.opc.posicionVisor).attr('class').split(" ");
			base.datos(vector,base.opc.posicionVisor);
			
		}
		
		
		base.moverCarrusel = function(){
			
			/*if(base.opc.mensajeModo2==true){
				$(base.opc.elementoMensajeModo2).hide();
				$(base.opc.elementoMensajeModo2).eq(base.contador).slideDown(1000);
			}
	
			if(base.opc.popup==true){
				var href=base.$el.find('> li').eq(base.contador).attr('title');
				$(base.opc.elementoPopup).attr('href',href);
			}*/
			
			//if(base.opc.paginadorPersonalizado==true) base.posicion = base.contador;
			
			//console.log("si 5  "+base.posicion);
			
			base.$el.stop('true','true').animate(
				{ 	left: -(base.desplazamiento * base.posicion)},
				{ 	queue: false, 
				  	duration: base.opc.velocidadCarrusel, 
				  	//easing: base.A_efectos[0], 
					complete: function(){
					  	if(base.opc.auto==true && base.moviendo==false){ 
							base.activarDelay(true);
						}
					} 
				}
				
			);
			
			/*if(base.opc.activarPaginacion==true){
				$(base.opc.paginador).find('a').removeAttr('class');
				$(base.opc.paginador).find('a').eq(base.contador).addClass('activo');
			}*/
			
			/*if(base.opc.activarPaginacionPersonalizado==true){
				
				$(base.opc.paginador).find('a').removeAttr('class');
				$(base.opc.paginador).find('a').eq(base.contador).addClass('activo');
			}*/
		
		}
		
		
		base.activarDelay = function(moviendo){
			
			if (moviendo !== true) { moviendo = false; }
			
			base.moviendo = moviendo;
			
			if (moviendo){
				
				base.desactivarDelay();
				base.delay = window.setInterval(function() {
					
					base.posicionVisor++;
					
					if(base.posicionVisor==base.totalElementos) base.posicionVisor = 0;

					console.log("posicion ->  "+base.$el.find(base.opc.elemento).eq(base.posicionVisor).attr('class'));
					var vector = base.$el.find(base.opc.elemento).eq(base.posicionVisor).attr('class').split(" ");
					base.datos(vector,base.posicionVisor);
					
					/*if(base.movDer==1) {
						console.log("si 1");
						base.movIzq=1;
						base.moverSlide();
						if(base.opc.botones==true){
							$(base.opc.botonIzq).removeClass(base.opc.botonIzqInactivo);
						}
					}
			
					if (base.desplazar == base.contador) {
			
						base.movIzq=1;
						base.movDer=0;
						
						//base.moverSlide();
						
						if(base.opc.botones==true){
							$(base.opc.botonIzq).removeClass(base.opc.botonIzqInactivo);
							$(base.opc.botonDer).addClass(base.opc.botonDerInactivo);
						}
						
					}
					
					if(base.contador>base.desplazar && base.desplazar!=0){
						base.contador = 0;
						base.moverSlide();
						console.log("si 2  "+base.desplazar);
						base.movIzq=0;
						base.movDer=1;
						if(base.opc.botones==true){
							$(base.opc.botonIzq).addClass(base.opc.botonIzqInactivo);
							$(base.opc.botonDer).removeClass(base.opc.botonDerInactivo);
						}
					}*/
					
					//console.log("si 3  "+base.contador);
					
				}, base.opc.temporizador);
			} else {
				base.desactivarDelay();
			}
			
		}
		
		
		base.desactivarDelay = function(){
			if (base.delay) { window.clearInterval(base.delay); }
		}
		
		
		base.activarBotones = function(){
			$(base.opc.botonIzqCarrusel).bind('click',base.moverIzq);
			$(base.opc.botonDerCarrusel).bind('click',base.moverDer);
		}
		
		base.moverIzq = function(){
			
			if(base.opc.auto==true) base.activarDelay(false);
			
			if(base.movIzq==1) {
				base.posicion--;	
				base.movDer=1;
				base.moverCarrusel();
				if(base.opc.botonesCarrusel==true){
					$(base.opc.botonDer).removeClass(base.opc.botonDerInactivo);
				}
			}
	
			if(base.posicion == 0) {
				base.movIzq=0;
				base.movDer=1;
				//base.contador = 0
				if(base.opc.botonesCarrusel==true){
					$(base.opc.botonIzq).addClass(base.opc.botonIzqInactivo);
				}
			}
			console.log("si boton izq  "+base.posicion+"    posicion Visor  "+base.posicionVisor);
		}
		
		base.moverDer = function(){
			
			if(base.opc.auto==true) base.activarDelay(false);
			
			if(base.movDer==1) {
				base.posicion++;
				base.movIzq=1;
				base.moverCarrusel();
				if(base.opc.botonesCarrusel==true){
					$(base.opc.botonIzqCarrusel).removeClass(base.opc.botonIzqInactivo);
				}
			}
			
			if(base.desplazar == base.posicion) {
				base.movDer=0;
				base.movIzq=1;
				if(base.opc.botonesCarrusel==true){
					$(base.opc.botonDerCarrusel).addClass(base.opc.botonDerInactivo);
				}
			}
			console.log("si boton der  "+base.posicion+"    posicion Visor  "+base.posicionVisor);
		}
		
		
		base.botonIzq=function(){
			if(base.opc.movIzq==1){
				base.opc.moverIzq();
				if (base.opc.movIzq== 0) {
					if(base.opc.botones==true){
						$(base.opc.botonIzqCarrusel).addClass(base.opc.botonIzqInactivo);
					}
				}
			}
			
		}
		
		base.botonDer=function(){
			if(base.opc.movDer==1){
				base.opc.moverDer();
				if(base.opc.movDer==0){
					if(base.opc.botones==true){
						$(base.opc.botonDerCarrusel).addClass(base.opc.botonDerInactivo);
					}
				}
			}
			
		}
		
		
		/* Publico */
		
		base.mover = function(x){
				
		}
		
		base.obtenerTotalElementos=function(){
			return base.totalElementos;
		}

		
		base.init();
		
	}
	
	$.AG_visor.porDefecto = {
		visor					: false,
		anchoVisor				: null,
		altoVisor				: null,
		activarPosicionInicial	: true,
		posicionVisor			: 0,
		mensaje					: false,
		elemento				: null,
		bloqueo					: null,
		auto					: false,
		temporizador			: 2000,
		botonesCarrusel			: false,
		botonIzqCarrusel		: null,
		botonDerCarrusel		: null,
		velocidadCarrusel		: 1000,
		desplazamiento			: 1,
		area					: 1
			
	};
	
	$.fn.AG_visor = function(opc) {

		if ((typeof(opc)).match('object|undefined')){
			return this.each(function(i){
				(new $.AG_visor(this, opc));
			});
		}

	};
	
	

})(jQuery);