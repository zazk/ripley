from django.conf.urls.defaults import patterns, include, url

from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'telvicom.views.home', name='home'),
    # url(r'^telvicom/', include('telvicom.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    
    # Nuevo ruteador
    url(r'^$',
    'apps.website.views.url_template', name='url_template'),
    url(r'^(?P<template>\w.*).html$',
    'apps.website.views.url_template', name='url_template'),

    # url(r'^$', 'apps.website.views.home', name='home'),
    # url(r'^noticias.html', 'apps.website.views.noticias', name='noticias'), 
    # url(r'^descargas.html', 'apps.website.views.descargas', name='descargas'),
    # url(r'^interesado.html', 'apps.website.views.interesado', name='interesado'),
    # url(r'^agenda.html', 'apps.website.views.agenda', name='agenda'),
    # url(r'^proceso.html', 'apps.website.views.proceso', name='proceso'),
    # url(r'^preuniversitaria.html', 'apps.website.views.preuniversitaria', name='preuniversitaria'),
    # url(r'^inscripcion.html', 'apps.website.views.inscripcion', name='inscripcion'),
    # url(r'^selectiva.html', 'apps.website.views.selectiva', name='selectiva'),

)
